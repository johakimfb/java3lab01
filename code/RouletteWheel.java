import java.util.Random;

public class RouletteWheel {
    private Random random;
    private int last_spin;

    public RouletteWheel() {
        this.random = new Random();
        this.last_spin = 0;
    }

    public void spin() {
        this.last_spin = this.random.nextInt(37);
    }

    public int getValue() {
        return this.last_spin;
    }
}