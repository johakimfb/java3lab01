import java.util.Scanner;

public class Roulette {
    
    public static boolean hasWon(int number, RouletteWheel wheel) {
        if (number == wheel.getValue())
        {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        boolean playing = true;
        int money = 1000;
        
        while (playing)
        {
            System.out.println("Would you like to make a bet (y/n): ");
            String wants_play = scanner.nextLine();
            if (wants_play.equals("n")) 
            {
                System.out.println("You won/lost " + (money - 1000) + "$");
                System.out.println("Goodbye!");
                playing = false;
                break;
            }
            
            System.out.println("How much money would you like to bet: ");
            int bet_ammount = scanner.nextInt();
            scanner.nextLine();
            if (bet_ammount > money || bet_ammount < 0)
            {
                System.out.println("Invalid betting ammount");
                continue;
            }

            System.out.println("Which number would you like to bet on (0 to 36): ");
            int number = scanner.nextInt();
            scanner.nextLine();
            if (number < 0 || number > 36)
            {
                System.out.println("The number must be between 0 and 36");
                continue;
            }
            
            wheel.spin();
            boolean won = hasWon(number, wheel);
            if (won) 
            {
                money += bet_ammount * 35;
                System.out.println("You won " + (bet_ammount * 35) + "$");
            }
            else
            {
                money -= bet_ammount;
                System.out.println("You lost " + bet_ammount + "$");
            }
        } 
        scanner.close();
    }
}
